package eu.telecomnancy.sensor;

import java.util.Observable;

public class AdapterSensor  implements ISensor {
	private LegacyTemperatureSensor T;
	private double temp;
	
	private AdapterSensor(LegacyTemperatureSensor U){
		this.T=U;
		temp=0;
		
	}
	
	
	public void on() {
		T.onOff();
		
		
	}

	@Override
	public void off() {
		T.onOff();
		
	}

	@Override
	public boolean getStatus() {
		return T.getStatus();
		
	}

	@Override
	public void update() throws SensorNotActivatedException {
		temp=T.getTemperature();
		
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return this.temp;
		
	}

}
