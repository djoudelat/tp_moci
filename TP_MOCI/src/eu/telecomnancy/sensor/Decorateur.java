package eu.telecomnancy.sensor;

public abstract interface Decorateur extends ISensor {
	public double getValue() throws SensorNotActivatedException;
	public void update() throws SensorNotActivatedException;
}
